-- ------------------------------------------------------------------------------------------------
-- Creation base de donnees
-- ------------------------------------------------------------------------------------------------

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema esport
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema esport
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `esport` DEFAULT CHARACTER SET utf8 ;
USE `esport` ;

-- -----------------------------------------------------
-- Table `esport`.`gamestyle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `esport`.`gamestyle` (
  `idgamestyle` INT NOT NULL AUTO_INCREMENT,
  `gamestyle` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idgamestyle`),
  UNIQUE INDEX `style_UNIQUE` (`gamestyle` ASC) VISIBLE)
ENGINE = InnoDB;


-- ------------------------------------------------------------------------------------------------
-- Creation compte utilisateur (2 comptes selon provenance)
-- ------------------------------------------------------------------------------------------------
-- Cree le user 
CREATE OR REPLACE USER 'dbuser_esport'@'%' IDENTIFIED BY 'dbuser_esport';

-- Droits pour le user sur la base de donnees 
GRANT SELECT,INSERT,UPDATE,DELETE,EXECUTE ON esport.* TO 'dbuser_esport'@'%';

-- Cree le user sur localhost
CREATE USER 'dbuser_esport'@'localhost' IDENTIFIED BY 'dbuser_esport';

-- Droits pour le user sur la base de donnees 
GRANT SELECT,INSERT,UPDATE,DELETE,EXECUTE ON esport.* TO 'dbuser_esport'@'localhost';

-- ------------------------------------------------------------------------------------------------


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
