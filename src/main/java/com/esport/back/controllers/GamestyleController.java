package com.esport.back.controllers;

import org.springframework.web.bind.annotation.*;
import java.util.Optional;
import java.util.List;

import com.esport.back.models.Gamestyle;
import com.esport.back.services.GamestyleService;



@RestController
@CrossOrigin(origins = {"*"}, maxAge = 4800, allowCredentials = "false")
@RequestMapping("/api/gamestyle")
public class GamestyleController {


    private final GamestyleService gamestyleService;

    public GamestyleController(GamestyleService gamestyleService) {
        this.gamestyleService = gamestyleService;
    }

    @PostMapping
    public Gamestyle createGamestyle(@RequestBody Gamestyle gamestyle) {
        return gamestyleService.createGamestyle(gamestyle);
    }

    @GetMapping
    public List<Gamestyle> getAllGamestyles() {
        return gamestyleService.getAllGamestyles();
    }

    @GetMapping("/{id}")
    public Optional<Gamestyle> getGamestyleById(@PathVariable Long id) {
        return gamestyleService.getGamestyleById(id);
    }

    @PutMapping("/{id}")
    public Gamestyle updateGamestyle(@PathVariable Long id, @RequestBody Gamestyle gamestyle) {
        return gamestyleService.updateGamestyle(id, gamestyle);
    }

    @DeleteMapping("/{id}")
    public void deleteGamestyle(@PathVariable Long id) {
        gamestyleService.deleteGamestyle(id);
    }

}
