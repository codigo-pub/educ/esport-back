package com.esport.back.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;



@Getter
@Setter
@Entity
@Table(name = "gamestyle" )
public class Gamestyle
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idgamestyle;

	@Column(name = "gamestyle", length=45)
	private String gamestyle;


}

