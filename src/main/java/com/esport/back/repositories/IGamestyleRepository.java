package com.esport.back.repositories;

import com.esport.back.models.Gamestyle;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.CrudRepository;
import java.util.List;


public interface IGamestyleRepository extends CrudRepository<Gamestyle,Long> {



}