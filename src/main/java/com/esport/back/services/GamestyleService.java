package com.esport.back.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.lang.NonNull;

import java.util.Optional;
import java.util.List;

import com.esport.back.models.Gamestyle;
import com.esport.back.repositories.IGamestyleRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class GamestyleService {
    
	static final Logger LOG = LoggerFactory.getLogger(Gamestyle.class);

    private final IGamestyleRepository gamestyleRepository;

    public GamestyleService(IGamestyleRepository gamestyleRepository) {
        this.gamestyleRepository = gamestyleRepository;
    }


    @Transactional
    public Gamestyle createGamestyle(@NonNull Gamestyle gamestyle) {
        return this.gamestyleRepository.save(gamestyle);
    }

    @Transactional(readOnly = true)
    public List<Gamestyle> getAllGamestyles() {
        return (List<Gamestyle>)this.gamestyleRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<Gamestyle> getGamestyleById(@NonNull Long id) {
        return this.gamestyleRepository.findById(id);
    }

    @Transactional
    public Gamestyle updateGamestyle(@NonNull Long id, @NonNull Gamestyle updatedGamestyle) {
        // Check if id is different from the one of model updated, we don't allow it
        if (!id.equals(updatedGamestyle.getIdgamestyle())) {
            throw new RuntimeException("Changing id is not allowed !");
        }

        // Check if data exists
        Optional<Gamestyle> optionalGamestyle = this.gamestyleRepository.findById(updatedGamestyle.getIdgamestyle());
        if (optionalGamestyle.isPresent()) {
            // Update data            
            return this.gamestyleRepository.save(updatedGamestyle);
        } else {
            throw new RuntimeException("Gamestyle not found with id " + updatedGamestyle.getIdgamestyle());
        }
    }

    @Transactional
    public void deleteGamestyle(@NonNull Long id) {
        Optional<Gamestyle> optionalGamestyle = this.gamestyleRepository.findById(id);
        if (optionalGamestyle.isPresent()) {
            this.gamestyleRepository.deleteById(id);
        } else {
            throw new RuntimeException("Gamestyle not found with id " + id);
        }
    }


    
}
